import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule, Http, RequestOptions, XHRBackend } from '@angular/http';

import { ColorPickerModule } from 'angular2-color-picker';

import { AppComponent } from './app.component';
import { ColorHelper } from './helpers/color_helper';
import { HttpService } from './services/http_service';
import { HelperService } from './services/helper_service';
import { TemplatesService } from './services/templates_service';

export function httpFactory(backend: XHRBackend, options: RequestOptions) {
  return new HttpService(backend, options);
}

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    ColorPickerModule
  ],
  providers: [
    ColorHelper, 
    TemplatesService,
    HelperService,
    {
      provide: HttpService,
      useFactory: httpFactory,
      deps: [XHRBackend, RequestOptions]
    },],
  bootstrap: [AppComponent]
})
export class AppModule { }
