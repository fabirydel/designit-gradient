export class Template {
  id: number;
  name: string;
  creator: string;
  style: string;
  direction: string;
  color_one: string;
  color_two: string;
  color_format: string;
}
