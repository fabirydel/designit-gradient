import { Injectable } from '@angular/core';

@Injectable()
export class HelperService {
  errors: Object = {};

  processErrors(errors): Object {
    this.errors = {};
    for (var key in errors) {
      this.errors[key] = (key.charAt(0).toUpperCase() + key.slice(1)).replace('_', ' ') + ' ' + errors[key][0];
    }
    return this.errors;
  }
}
