import { Injectable, Inject } from '@angular/core';
import { Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Template } from '../models/template';
import { HttpService } from '../services/http_service';
import { environment } from '../../environments/environment';

@Injectable()
export class TemplatesService {
  private templatesUrl = environment.APIUrl + '/templates';
  private userActivitiesUrl = environment.APIUrl + '/user_activities';

  constructor(private http: HttpService) {}

  create(template: Template): Observable<Template> {
    return this.http.post(this.templatesUrl, {'template': template})
      .map(this.http.extractData)
      .catch(this.http.handleError);
  }

  index(): Observable<Template> {
    return this.http.get(this.templatesUrl)
      .map(this.http.extractData)
      .catch(this.http.handleError);
  }

  delete(templateId: number): Observable<Template> {
    return this.http.delete(this.templatesUrl + '/' + templateId)
      .map(this.http.extractData)
      .catch(this.http.handleError);
  }
}
