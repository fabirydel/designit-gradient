import { Injectable } from '@angular/core';
import { Http, XHRBackend, RequestOptions, RequestOptionsArgs, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

@Injectable()
export class HttpService extends Http {

  constructor (backend: XHRBackend, options: RequestOptions) {
    super(backend, options);
  }

  request(url: any, options?: RequestOptionsArgs): Observable<Response> {
    if (typeof url === 'string') { // meaning we have to add the token to the options, not in url
      if (!options) {
        // let's make option object
        options = { 
          headers: new Headers() 
        };
      }
    } else {
      if (!(url._body instanceof FormData)) {
        url.headers.set('Content-Type', 'application/json');
      }
      // we have to add the token to the url object
    }
    return super.request(url, options).catch(this.catchAuthError(this));
  }

  extractData(res: Response) {
    let body = res.json();
    return body || { };
  }

  public handleError(error: Response | any) {
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.errors || JSON.stringify(body);
      errMsg = err;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    return Observable.throw(errMsg);
  }

  private catchAuthError(self: HttpService) {
    // we have to pass HttpService's own instance here as `self`
    return (res: Response) => {
      console.log(res);
      if (res.status === 401 || res.status === 403 || res.status === 400) {
        // if not authenticated
        console.log(res);
      }
      return Observable.throw(res);
    };
  }

  getItemFromStorage(item) {
    let user = JSON.parse(localStorage.getItem('user'));
    return user == null ? null : user[item];
  }
}
