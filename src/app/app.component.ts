import { Component } from '@angular/core';
import { ColorPickerService } from 'angular2-color-picker';
import { DomSanitizer } from '@angular/platform-browser';
import { ColorHelper } from './helpers/color_helper';

import { TemplatesService } from './services/templates_service';
import { HelperService } from './services/helper_service';
import { Template } from './models/template';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  styles: string[] = ['linear', 'radial'];
  directions: string[] = ['top', 'top right', 'right', 'bottom right', 
    'bottom', 'bottom left', 'left', 'top left'];
  color_formats: string[] = ['hex', 'rgba'];
  vendor_prefixes: string[] = ['-webkit-', '-o-', '-moz-', ''];

  display_color: any = '';

  templates: Template[];
  template: Template = new Template;
  errors: Object = {};

  constructor(private colorPickerService: ColorPickerService, private domSanitizer: DomSanitizer, 
    private colorHelper: ColorHelper, private templatesService: TemplatesService,
    private helperService: HelperService) {}

  ngOnInit() {
    this.template.style = 'linear';
    this.template.direction = 'top';
    this.template.color_format = 'hex';
    this.template.color_one = '#74e3ec';
    this.template.color_two = '#c7ffe2';
    this.updateDisplayColor();
    this.loadTemplates();
  }

  selectStyle(style) {
    this.updateStyle(style);
    this.updateDisplayColor();
  }

  selectDirection(direction) {
    this.template.direction = direction;
    this.updateDisplayColor();
  }

  selectColorFormat(color_format) {
    if (this.template.color_format != color_format) {
      if (color_format == 'rgba') {
        this.template.color_one = this.colorHelper.hexToRgb(this.template.color_one);
        this.template.color_two = this.colorHelper.hexToRgb(this.template.color_two);
      } else {
        this.template.color_one = this.colorHelper.rgbToHex(this.template.color_one);
        this.template.color_two = this.colorHelper.rgbToHex(this.template.color_two);
      }
      this.template.color_format = color_format;
      this.updateDisplayColor();
    }
  }

  changeColorOne(color) {
    this.template.color_one = color;
    this.updateDisplayColor();
  }

  changeColorTwo(color) {
    this.template.color_two = color;
    this.updateDisplayColor();
  }

  updateDisplayColor() {
    let basicStyle = `${ this.template.style }-gradient(${ this.template.direction }, ${ this.template.color_one }, ${ this.template.color_two })`;
    this.display_color = this.domSanitizer.bypassSecurityTrustStyle(`
      background: -o-${basicStyle};
      background: -moz-${basicStyle};
      background: -webkit-${basicStyle};
      background: ${basicStyle};`
    );
  }

  updateStyle(style) {
    if (style == 'radial' && this.template.style != 'radial') {
      this.directions.unshift('center');
      this.template.direction = 'center';
    } else if (style == 'linear' && this.template.style != 'linear') {
      var index = this.directions.indexOf('center', 0);
      if (index > -1) {
        this.directions.splice(index, 1);
        this.template.direction = 'top';
      }
    }
    this.template.style = style;
  }

  convertColorFormat(event) {
    if (this.template.color_format == 'rgba') {
      return this.colorHelper.hexToRgb(event);
    } else if(event[0] != '#') {
      return this.colorHelper.rgbToHex(event);
    } else {
      return event;
    }
  }

  selectTemplate(template) {
    this.template.direction = template.direction;
    this.template.color_format = template.color_format;
    this.template.color_one = template.color_one;
    this.template.color_two = template.color_two;
    this.updateStyle(template.style);
    this.updateDisplayColor();
  }

  addTemplate() {
    this.templatesService.create(this.template).subscribe(
      data => {
        this.errors = {};
        this.loadTemplates();
      },
      error => this.errors = this.helperService.processErrors(error)
    );
  }

  loadTemplates() {
    this.templatesService.index().subscribe(
      data => {
        this.templates = data['templates'];
      }
    );
  }

  deleteTemplate(template) {
    this.templatesService.delete(template.id).subscribe(
      data => {
        this.loadTemplates();
      }
    );
  }
}
