import { Injectable } from '@angular/core';

@Injectable()
export class ColorHelper {

  rgbToHex(rgb) {
    let rgbRegex = /^rgb\(\s*(-?\d+)(%?)\s*,\s*(-?\d+)(%?)\s*,\s*(-?\d+)(%?)\s*\)$/;
    let result, r, g, b, hex = '';
    if ( (result = rgbRegex.exec(rgb))) {
      r = this.componentFromStr(result[1], result[2]);
      g = this.componentFromStr(result[3], result[4]);
      b = this.componentFromStr(result[5], result[6]);
      hex = '#' + (0x1000000 + (r << 16) + (g << 8) + b).toString(16).slice(1);
    }
    return hex;
  }

  componentFromStr(numStr, percent) {
    let num = Math.max(0, parseInt(numStr, 10));
    return percent ? Math.floor(255 * Math.min(100, num) / 100) : Math.min(255, num);
  }

  hexToRgb(hex) {
    let c = hex.substring(1).split('');
    if (c.length== 3) {
      c = [c[0], c[0], c[1], c[1], c[2], c[2]];
    }
    c = '0x' + c.join('');
    return 'rgb(' + [(c >> 16) & 255, (c >> 8) & 255, c & 255].join(',') + ')';
  }
}
