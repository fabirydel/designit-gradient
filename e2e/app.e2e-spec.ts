import { DesignitGradientPage } from './app.po';

describe('designit-gradient App', () => {
  let page: DesignitGradientPage;

  beforeEach(() => {
    page = new DesignitGradientPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
